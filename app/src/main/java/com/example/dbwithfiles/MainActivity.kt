package com.example.dbwithfiles

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.nio.charset.StandardCharsets

class MainActivity : AppCompatActivity() {
    val db = MyDBHelper(this@MainActivity,null)
    //Достаем файл из телефона, записываем его в базу и выводим его содержимое
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1 && null != data && resultCode == RESULT_OK){
            val uri = Uri.parse(data.data!!.toString())
            val file = File(uri.path!!)
            var filebytearray = contentResolver.openInputStream(uri)!!.readBytes()
            val form = file.name.substringAfter(".")
            db.adFile(filebytearray, form)
            val dbfile = db.getFile()
            val str = String(dbfile, StandardCharsets.UTF_8)
            text.setText(str)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setType("*/*")
        startActivityForResult(intent, 1)

        //Файл из assets
        //val file = applicationContext.assets.open("test2.txt").readBytes()
        //db.adFile(file, "txt")
        //text.setText(db.getFile())
    }
}