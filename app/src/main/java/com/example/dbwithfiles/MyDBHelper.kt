package com.example.dbwithfiles

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.nio.charset.StandardCharsets

class MyDBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?): SQLiteOpenHelper(context,
    DATABASE_NAME, factory, DATABASE_VERSION)
{
    companion object{
        private val DATABASE_NAME = "FileDB2"
        private val DATABASE_VERSION = 1
        val TABLE_NAME = "FileTable"
    }

    override fun onCreate(p0: SQLiteDatabase?) {
        val query = ("CREATE TABLE $TABLE_NAME (fileByte varbinary(8000) not null, format varchar(10) not null)")
        p0?.execSQL(query)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        p0?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(p0)
    }
    fun adFile(file: ByteArray, format: String){
        val values = ContentValues()
        values.put("fileByte", file)
        values.put("format", format)
        val db = this.writableDatabase
        db.insert(TABLE_NAME,null,values)
        db.close()
    }
    fun getFile(): ByteArray{
        val db = this.readableDatabase
        var filetxt: String = " "
        var fileformat: String = " "
        var filebyte: ByteArray = byteArrayOf()
        val result = db.rawQuery("SELECT * FROM $TABLE_NAME" , null)
        if (result.moveToLast()){
            filebyte = result.getBlob(0)
            fileformat = result.getString(1)
        }
        return filebyte
    }
}